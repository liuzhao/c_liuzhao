#include "stdafx.h"
#include "string.h"
#include "stdlib.h"                          
#define A "There has an error in the line of "                  //定义字符串常量A,B方便result.txt文件的分行
#define B "**********************************\n\r"

bool right_or_not (char * a,char * b);

int _tmain(int argc, _TCHAR* argv[])
{
	FILE * f1;
	FILE * f2;
	int count_line = 0;

	if ((f1 = fopen("English.txt", "r")) == NULL )
		{
			printf("can't open file");
			exit(1);
	    }
	if ((f2 = fopen("Chinese.txt", "r")) == NULL )
		{
			printf("can't open file");
			exit(1);
	    }

	char file1_line[1024] = {0};                               //用数组file1_line[]存放每一行中的文件的字符串，一行最多1024个字符
	char file2_line[1024] = {0};
	while (fgets(file1_line, 1024, f1) != NULL && fgets(file2_line, 1024, f2) != NULL && file1_line[0] != '\0' && file2_line[0] != '\0' )   //当两个文本文件每一行都存在时并且不为空时
	{
		count_line++;
		if (right_or_not(file1_line, file2_line))                    //通过判断函数决定接下来的操作，如果相同，则返回循环
			continue;
		else
		{
			FILE * f3;
			f3 = fopen("C:\\Users\\admin\\Desktop\\result.txt", "a");  //如果不同，在桌面上生成结果文件result.txt,并将输出结果写入文件
			fwrite(&B, strlen(B), 1, f3);
			fwrite(&A, strlen(A), 1, f3);
			fprintf(f3, "%d\n\r", count_line );
			fwrite(file1_line, strlen(file1_line), 1, f3);
			fwrite(file2_line, strlen(file1_line), 1, f3);
			
			fclose(f3);
		}	
	}
	return 0;
}

bool right_or_not (char * a,char * b)  //比较函数，返回bool型,找出两个文本文件中某同一行%后字母，写入数组cun1和cun2，判断是否相同
{
	char cun1[10] = {0};               //将数组初始化为0       
    char cun2[10] = {0};
	int count = 0;
	int i = 0;
	while (a[i] != '\0')               //检测是否到行尾
	{
		if (a[i] == '%')               //检测是否存在% 
		{
			cun1[count] = a[i+1];      //将%号后面的字符分别存入cun1[] 和cun2[]数组
			count++;
		}
		i++;
	}
	count = 0;
	i = 0;
	while (b[i] != '\0')
	{
		if (b[i] == '%')                
		{
			cun2[count] = b[i+1];
			count++;
		}
		i++;
	}
	if (strcmp(cun1,cun2) == 0 )        //比较数组是否相同
		return true;
	else 
		return false;

}